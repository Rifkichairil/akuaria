<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>srtdash - ICO Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{asset('strdash/images/icon/favicon.ico')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/metisMenu.css')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/slicknav.min.css')}}">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="{{asset('strdash/css/typography.css')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/default-css.css')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('strdash/css/responsive.css')}}">
    @yield('style')
    <!-- modernizr css -->
    <script src="{{asset('strdash/js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
          @include('layouts.admin.pages.sidebar')
        </div>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            @include('layouts.admin.pages.header')
            <!-- header area end -->
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Dashboard</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            <img class="avatar user-thumb" src="{{asset('strdash/images/author/avatar.png')}}" alt="avatar">
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown">Kumkum Rai <i class="fa fa-angle-down"></i></h4>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Message</a>
                                <a class="dropdown-item" href="#">Settings</a>
                                <a class="dropdown-item" href="#">Log Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
               @yield('content')
            </div>
        </div>
        <!-- main content area end -->
        <!-- footer area start-->

        <!-- footer area end-->
    </div>
    <!-- page container area end -->
    <!-- offset area start -->
    <div class="offset-area">
       @include('layouts.admin.pages.offside')
    </div>
    <!-- offset area end -->
    <!-- jquery latest version -->
    <script src="{{asset('strdash/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{asset('strdash/js/popper.min.js')}}"></script>
    <script src="{{asset('strdash/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('strdash/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('strdash/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('strdash/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('strdash/js/jquery.slicknav.min.js')}}"></script>

    <!-- start chart js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <!-- start highcharts js -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <!-- start zingchart js -->
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
    <script>
    zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
    </script>
    <!-- all line chart activation -->
    <script src="{{asset('strdash/js/line-chart.js')}}"></script>
    <!-- all pie chart -->
    <script src="{{asset('strdash/js/pie-chart.js')}}"></script>
    <!-- others plugins -->
    <script src="{{asset('strdash/js/plugins.js')}}"></script>
    <script src="{{asset('strdash/js/scripts.js')}}"></script>
    @yield('script')
</body>

</html>

