@extends('layouts.app')

@section('content')
<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope d-flex align-items-center"><a href="mailto:akuariza@gmail.com">akuariza@gmail.com</a></i>
        <i class="bi bi-phone d-flex align-items-center ms-4"><span>+62 857 1653 6178</span></i>
      </div>
      <div class="social-links d-none d-md-flex align-items-center">
        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

      <!--<h1 class="logo"><a href="index.html">BizLand<span>.</span></a></h1>-->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="" class="logo"><img src="{{ asset('/img/Presentation1.png') }}" alt=""></a>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="{{ route('index') }}">BERANDA</a></li>
          <li class="dropdown"><a href=""><span>TENTANG KAMI</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="">Akuariza</a></li>
              <li><a href="">Manfaat Akuariza</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="">PRODUK</a></li>
          <li><a class="nav-link scrollto" href="#cabang">GERAI</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <div class="d-flex">
        <a href="#about" class="btn-get-started scrollto">Get Started</a>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="" class="featured-services">
      <div class="container" data-aos="fade-up">

        <div class="row">
          

          <div class="col-md-9 col-lg-6 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4 class="title"><a href="">Perjalanan Akuariza</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
            </div>
          </div>

          <div class="col-md-9 col-lg-6 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4 class="title"><a href="">Manfaat Akuariza</a></h4>
              <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
            </div>
          </div>

          

        </div>

      </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->
    <section id="#about" class="about section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Tentang Kami</h2>
          <h3>Air Minum Pilihan</h3><br><br>
        </div>
        <div class="row">
          <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
            <img src="{{ asset('/img/about.jpg') }}" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
            <h3><span>AKUARIZA</span></h3>
            <p class="font-italic">
              Diambil dari mata air pegunungan Ciapus yang diolah dan disaring dengan 2x penyaringan sehingga terjamin kebersihan dan kualitasnya.
            </p>
            <ul>
              <li>
                <i class="bx bx-store-alt"></i>
                <div>
                  <h5>Bersih dan berkualitas</h5>
                  <p>Air Akuariza memiliki air yang berkualitas dengan sensasi menyegarkan membuat hidrasimu terjaga.</p>
                </div>
              </li>
              <li>
                <i class="bx bx-images"></i>
                <div>
                  <h5>Sehat Alami</h5>
                  <p>Air Akuariza diambil dari pegunungan Ciapus yang Sehat dan alami.</p>
                </div>
              </li>
            </ul>
            <p>
              Depo air minum Akuariza memberikan layanan pengisian galon di lokasi gerai yang tersebar di berbagai tempat. Akuariza menyediakan air minum dengan harga terjangkau dan mudah didapatkan. Depo air minum Akuariza sudah menyediakan layanan ambil/antar galon di masing masing gerai.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container" data-aos="zoom-in">

        <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{ asset('/img/testimonials/testi1.png') }}" class="testimonial-img" alt="">
                <h3>Jenal</h3>
                <h4>Pelanggan</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                 Rasa air nya enak dan segar, harga terjangkau dengan pelayanan yang prima.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{ asset('/img/testimonials/testi1.png') }}" class="testimonial-img" alt="">
                <h3>Elin</h3>
                <h4>Pelanggan</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Worth banget, harga murah cocok dengan kantong. Rasa air nya nyegerin. Top deh!
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="{{ asset('/img/testimonials/testi1.png') }}" class="testimonial-img" alt="">
                <h3>Aldi</h3>
                <h4>Pelangga</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Harga yang terjangkau dengan kualitas bagus, cocok dengan kantong pula. Pelayanan bagus, dan mudah didapatkan.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Team Section ======= -->
    <section id="cabang" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Gerai</h2>
          <h3><span>Akuariza</span></h3>
          <p>Akuariza memiliki beberapa gerai yang tersebar di daerah Jabodetabek diantaranya</p>
        </div>

        <div class="row">

          
          <div class="col-lg-4 col-md-7 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="member">
              <div class="member-img">
                <img src="{{ asset('/img/team/team-2.jpg') }}" class="img-fluid" alt="">
              </div>
              <div class="member-info">
                <h4>Akuariza 2</h4>
                <span>Product Manager</span>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-7 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="member-img">
                <img src="{{ asset('/img/team/team-3.jpg') }}" class="img-fluid" alt="">
              </div>
              <div class="member-info">
                <h4>Walfa Water</h4>
                <span>CTO</span>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-7 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="400">
            <div class="member">
              <div class="member-img">
                <img src="{{ asset('/img/team/team-4.jpg') }}" class="img-fluid" alt="">
              <div class="member-info">
                <h4>Giri Water</h4>
                <span>Accountant</span>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->



  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container py-4">
      <div class="copyright">
        &copy; Copyright <strong><span>2021</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/bizland-bootstrap-business-template/ -->
        Akuariza
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('/vendor/aos/aos.js') }}"></script>
  <script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('/vendor/glightbox/js/glightbox.min.js') }}"></script>
  <script src="{{ asset('/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('/vendor/purecounter/purecounter.js') }}"></script>
  <script src="{{ asset('/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{ asset('/vendor/waypoints/noframework.waypoints.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('/js/main.js') }}"></script>

</body>

</html>

@endsection